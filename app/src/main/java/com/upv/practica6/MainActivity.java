package com.upv.practica6;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;


public class MainActivity extends ActionBarActivity {
    Button save,load;
    EditText message;
    EditText message2;
    EditText message3;
    String Message;
    int data_block=100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        save = (Button) findViewById(R.id.SAVE);
        load = (Button) findViewById(R.id.Load);
        message = (EditText) findViewById(R.id.msg);
        message2 = (EditText) findViewById(R.id.msg2);
        message3 = (EditText) findViewById(R.id.msg3);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Message = message.getText().toString() + " "+ message2.getText().toString() + " " + message3.getText().toString();
                try {
                    FileOutputStream fou = openFileOutput("text.txt", MODE_WORLD_READABLE);
                    OutputStreamWriter osw = new OutputStreamWriter(fou);
                    try {
                        osw.write(Message);
                        osw.flush();
                        osw.close();
                        Toast.makeText(getBaseContext(), "Dato guardado", Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

        load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    FileInputStream fis = openFileInput("text.txt");
                    InputStreamReader isr = new InputStreamReader(fis);
                    char[] data = new char[data_block];
                    String final_data = "";
                    int size;
                    try {
                        while ((size = isr.read(data)) > 0) {
                            String read_data = String.valueOf(data, 0, size);
                            final_data += read_data;
                            data = new char[data_block];
                        }
                        Toast.makeText(getBaseContext(), "Message :" + final_data, Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
